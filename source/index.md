---
layout: home
full_title: Drupal Media
nav: home
---

<div class="jumbotron">
  <h2>Drupal 8 Core Media Initiative</h2>
  <p>Dries Buytaert declared Media as a top priority at DrupalCon New Orleans. The Media contributors are gathering a team and a plan for an official Drupal Core initiative to improve media handling in Drupal 8!</p>
  <p>
    <a class="btn btn-primary btn-lg" href="{{ site.url }}/contribute" role="button"><i class="fa fa-users" aria-hidden="true"></i> Get involved</a>
    <a class="btn btn-success btn-lg" href="{{ site.url }}/donate" role="button"><i class="fa fa-usd" aria-hidden="true"></i> Help fund</a>
  </p>
</div>


# This is the homepage!

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus consectetur ante tincidunt ligula laoreet faucibus. Fusce cursus nisl id mattis ultricies. Donec laoreet quam at nulla molestie, et maximus risus mattis. Nullam id dui tincidunt, convallis lectus vel, condimentum libero. Phasellus quis mauris eu nisl commodo malesuada et eget libero. Aenean in sagittis nisi. In hac habitasse platea dictumst. Donec mattis, purus at vestibulum efficitur, mauris risus ultrices purus, ultricies egestas elit mauris nec orci. Suspendisse venenatis magna quis feugiat vestibulum. Vestibulum sit amet erat gravida, bibendum sapien vel, aliquet lacus. Pellentesque id turpis sed justo congue sollicitudin. Praesent sed sodales sapien, eget scelerisque leo. Vivamus sit amet magna quam. Praesent fermentum, diam quis sagittis pharetra, justo nulla faucibus ante, in imperdiet ipsum turpis vel nisl. Proin lobortis hendrerit malesuada. Cras at laoreet ipsum.

<div class="row">
  <div class="col-xs-12 col-md-4">
    <div class="thumbnail" style="text-align: center">
      <img src="http://www.mmsholdings.com/media/1115/june-2012.jpg" alt="..." style="height: 200px; width: 100%" />
      <div class="caption">
        <h3>Reusable</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus consectetur ante tincidunt ligula laoreet faucibus.</p>
      </div>
    </div>
  </div>
  <div class="col-xs-12 col-md-4">
    <div class="thumbnail">
      <img src="https://spiritualmusclehead.files.wordpress.com/2014/06/puzzle-pieces.jpg" alt="..." style="height: 200px; width: 100%" />
      <div class="caption" style="text-align: center">
        <h3>Flexible</h3>
        <p>Curabitur ac nibh dui. Phasellus maximus, sapien eget sagittis ultricies, metus felis dictum lectus, et placerat ante risus at velit.</p>
      </div>
    </div>
  </div>
  <div class="col-xs-12 col-md-4">
    <div class="thumbnail">
      <img src="http://www.black.ninja/wp-content/uploads/2016/03/usability.jpg" alt="..." style="height: 200px; width: 100%" />
      <div class="caption" style="text-align: center">
        <h3>Mobile and Usable</h3>
        <p>Aliquam porttitor elit eget sagittis volutpat. Suspendisse et odio risus. Aliquam sagittis placerat nisl, nec semper mi aliquet id.</p>
      </div>
    </div>
  </div>
</div>

## The Media Team

Some kind of short description about the team. Curabitur ac nibh dui. Phasellus maximus, sapien eget sagittis ultricies, metus felis dictum lectus, et placerat ante risus at velit. Integer quis nisl non diam fermentum molestie non convallis arcu. Maecenas efficitur tempus risus, sed ultrices eros. Morbi ut sapien lacus. Pellentesque efficitur justo sit amet libero rutrum, quis molestie felis sagittis. Integer vel turpis ut tellus vestibulum dignissim in eu tortor. Maecenas accumsan tempus nibh, eu iaculis tortor commodo at. Donec ultricies sodales aliquam. Mauris auctor ipsum sit amet nunc dictum, eget dapibus urna blandit.
