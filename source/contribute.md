---
layout: default
title: Contribute
nav: contribute
---

# Contribute

The issues currently being worked on by our team are marked with 'Media Initiative' and 'sprint' in the drupal.org issue queue. Come to our weekly meetings to discuss what to work on and get help with any issues you are working on.

See all the issues we are working on at <a href="http://contribkanban.com/#/sprint/d8Media">the initiative's Kanban board on contribkanban.com</a>

## Weekly Scrum and Office Hours

The Media initiative's regular meeting time is <a href="http://www.timeanddate.com/worldclock/fixedtime.html?msg=Drupal+Media+Scrum&amp;iso=20150805T14&amp;p1=1440&amp;pm=00">14:00-15:00 UTC</a> every Wednesday. You don't need to commit to come every Wednesday.

Meetings are held over IRC in the #drupal-media channel. See <a href="http://drupal.org/irc">http://drupal.org/irc</a> for more information. Prior meeting times as well as IRC logs are accessible in this calendar additionally to future events. Subscribe to this calendar in Google Calendar as <a href="mailto:{{ site.calendar_id }}">{{ site.calendar_id }}</a> or using any iCal-compatible method with <a href="{{ site.calendar_ical }}">{{ site.calendar_ical }}</a>.</p>

## Sprints

The Media team regularly organizes and attends sprints at major Drupal events, such as DrupalCon.

<!--## Calendar

<iframe src="https://www.google.com/calendar/embed?src={{ site.calendar_id }}&showTitle=0&showPrint=0&showCalendars=0" style="border: 0" width="100%" height="600" frameborder="0" scrolling="no"></iframe>-->
